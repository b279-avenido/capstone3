



import { useContext } from "react"
import UserContext from "../UserContext";
import { NavLink } from "react-router-dom";




import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";


export default function AppNavbar(){

    const { user } = useContext(UserContext);
    const userFullName = `${user.firstName} ${user.lastName}`
    console.log(user);
    return(
        <Navbar expand="lg"  className="shadow bg-white sticky-top m-0 px-2 text-white" id="AppNavbar">
        <Container fluid>
            <Navbar.Brand as={ NavLink } to="/" end className="font-weight-bold">
            CA
        </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto ">
            
            <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
            <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>

            {

                (user.token !== null)
                ?
                <>
                 <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
                 
                </>
                :
                <>
                
                <Nav.Link as={ NavLink } to="/login" end>Login</Nav.Link>
                <Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>

                
                </>



            }
            {
                (user.isAdmin)
                ?
                <Nav.Link as={ NavLink } to="/dashboard" end>Dashboard</Nav.Link>
                :
                <>
                </>
            }

            


        </Nav>

            </Navbar.Collapse>
        </Container>
        </Navbar>
    )
}

