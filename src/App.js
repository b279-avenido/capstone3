import './App.css';
import AppNavbar from "./components/AppNavbar"
import Home from "./pages/Home"
import Product from "./pages/Products"
import Register from "./pages/Register"
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import Error from "./pages/Error"
import ProductView from "./components/ProductView"
import Dashboard from"./pages/Dashboard"
import AllProducts from "./pages/AllProducts"
import CheckOut from './pages/CheckOut';


import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Container } from "react-bootstrap";
import { } from "react-bootstrap"
import { useState, useEffect } from "react"
import { UserProvider } from "./UserContext"

function App() {

  //    const [user, setUser] = useState({
  //     id: null,
  //     isAdmin: null,
  //     email: null,
  //     token: localStorage.getItem("token")
  // })

  // const [user, setUser] = useState({
  //   id: localStorage.getItem("id"),
  //   email: localStorage.getItem("email"),
  //   token: localStorage.getItem("token"),
  //   isAdmin: localStorage.getItem("isAdmin")
  // })

  //    const unsetUser = () => {
  //   localStorage.clear();
  // }

  //   useEffect(() => {
  //   console.log(user);
  //   console.log(localStorage);
  // }, [user])


const [user, setUser] = useState({
  id: null,
  isAdmin: true,  //isAdmin: null,
  email: null,
  token: localStorage.getItem("token")
  
})

// Function for clearing the storage on logout
const unsetUser = () =>{
    localStorage.clear(); 
  }

  useEffect(()=>{
    console.log(user);
    console.log(user);
    console.log(localStorage);
  }, [user])
  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data._id !== undefined){
        console.log(" makuha");
        setUser({
            id: data._id,
            firstName : data.firstName,
            lastName : data.lastName,
            email : data.email,
            mobileNo : data.mobileNumber,
            isAdmin: data.isAdmin
        });
      }
      else{
        console.log("di makuha");
        setUser({
          
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])


  return (
    
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
        <Container>
         <Routes>
           <Route path="/" element={<Home/>}/>
           <Route path="/products" element={<Product/>}/>
           <Route path="/productView/:productId" element={<ProductView/>}/>
           <Route path="/register" element={<Register/>}/>
           <Route path="/login" element={<Login/>}/>
           <Route path="/logout" element={<Logout/>}/>
           <Route path="/dashboard" element={<Dashboard/>}/>
           <Route path="/products/allProducts" element={<AllProducts/>}/>
           <Route exact path="/products/buy/:productId" element={<CheckOut />} />
           <Route path="*" element={<Error/>}/>
         </Routes>
        </Container>
    </Router>
  </UserProvider>
    
  );
}

export default App;
