import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import Banner from "../components/Banner";
import HomeCarousel from "../components/HomeCarousel";
import HotDeals from "../components/HotDeals";

export default function Error(){

	const { user } = useContext(UserContext);

	const data = {
		title: "404 - NOT FOUND",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Home"
	}

	return(
		<>
		{
		(user.isAdmin)
		?
		<Navigate to="/dashboard" />
		:
		<Banner bannertop={data} />
		}
		</>
		
		);
}
